import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormPseudoComponent } from './form-pseudo.component';

describe('FormPseudoComponent', () => {
  let component: FormPseudoComponent;
  let fixture: ComponentFixture<FormPseudoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormPseudoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormPseudoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
