import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormsModule, NgForm, FormControl, FormArray } from '@angular/forms';

@Component({
  selector: 'app-form-pseudo',
  templateUrl: './form-pseudo.component.html',
  styleUrls: ['./form-pseudo.component.scss']
})
export class FormPseudoComponent implements OnInit {

  model: FormGroup;
  public isSubmit: boolean = false;
  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
      this.model = this.formBuilder.group({
      pseudo:''
    });
  }

  sendForm(): void {
    this.savePseudo();
    this.isSubmit = true;
  }

  savePseudo(){
    sessionStorage.setItem('pseudo',this.model.value.pseudo);
  }

}
