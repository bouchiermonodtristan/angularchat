import { Component, Output, EventEmitter, Input } from '@angular/core';
import { Channel } from "../channel";

@Component({
  selector: 'app-channel',
  templateUrl: './channel.component.html',
  styleUrls: ['./channel.component.scss']
})
export class ChannelComponent implements OnInit {

  constructor() { }

  @Input()
  channel: Channel;


}
