import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ChatMessage } from './chat-message';
import { Channel } from "./channel";

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient) {}

  getChatMessages(): Observable<ChatMessage[]> {
    return this.http.get<ChatMessage[]>(environment.backUrl + '/messages')
  }
  getRoomlist(): Observable<Channel[]> {
    return this.http.get<Channel[]>(environment.backUrl + '/channels')
  }
  updateChatMessage(chatMessage: ChatMessage): Observable<void> {
    return this.http.put<void>(environment.backUrl + '/messages/${chatMessage.id}', chatMessage)
  }
  createChatMessage(chatMessage: ChatMessage): Observable<ChatMessage> {
    return this.http.post<ChatMessage>(environment.backUrl + '/messages', chatMessage)
  }
}
