import { Component, OnInit } from "@angular/core";
import { ChatMessage } from "../chat-message";
import { HttpService } from "../http.service";

@Component({
  selector: 'app-chat-message-list',
  templateUrl: './chat-message-list.component.html',
  styleUrls: ['./chat-message-list.component.scss']
})
export class ChatMessageListComponent implements OnInit {

  chatMessages: ChatMessage[] = [];

  newChatMessage: string;

  constructor(private myService: HttpService) { }

  ngOnInit() {
      this.refreshChatMessages();
  }

  private refreshChatMessages() {
    this.myService.getChatMessages().subscribe((chatMessages:ChatMessage[]) => this.chatMessages = chatMessages);
  }

  addChatMessage(chatMessage: ChatMessage)
  {
  const newChatMessage: ChatMessage = {
      message : this.newChatMessage,
      pseudo: sessionStorage.getItem('pseudo')
  }
    this.myService.createChatMessage(newChatMessage).subscribe((chatMessage: ChatMessage) =>{this.chatMessages.push(chatMessage)});
  }

  updateChatMessage(chatMessage: ChatMessage)
  {
    this.myService.updateChatMessage(chatMessage).subscribe();
  }

}
