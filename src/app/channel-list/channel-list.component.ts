import { Component, OnInit } from '@angular/core';
import { Channel } from "../channel";
import { HttpService } from "../http.service";

@Component({
  selector: 'app-channel-list',
  templateUrl: './channel-list.component.html',
  styleUrls: ['./channel-list.component.scss']
})
export class ChannelListComponent implements OnInit {

  channels: Channel[] = [];

  constructor(private myService: HttpService) { }

    ngOnInit() {
      this.getChannelList();
  }

    private getChannelList() {
    this.myService.getRoomlist().subscribe((channels:Channel[]) => this.channels = channels);
  }

}
